import { Timeouts } from '../../../infra/enum/timeouts';
import { BaseComponent } from '../../../infra/models/base-compents';
import { waitUntil } from '../../../infra/waiter/wait';
import { BasePage } from '../base-Page';

export class Facebook extends BasePage {

    private get popUpValidation() { return $('//button[contains(text(), "Accept All")]') }

    private get userId() { return $('#email') }

    private get password() { return $('#pass') }

    public isUserLoggedIn(): boolean{
		const localStorage = browser.getLocalStorage();
		const isUserLoggedIn = localStorage.includes('knAdminauth');
		return isUserLoggedIn;
    }
    
    public isFaceBookPopUpExisting(): boolean {
        return waitUntil(() => this.popUpValidation.isExisting(), Timeouts.FORTY_SECONDS, 'Facebook pop was not displaying');
    }

    public clickOnFaceBookPopUp() {
        waitUntil(() => this.popUpValidation.isExisting(), Timeouts.FORTY_SECONDS, 'Facebook pop was not displaying');
        this.popUpValidation.click();
    }

    public isUserIdTextBoxExisting(): boolean {
        return waitUntil(() => this.userId.isExisting(), Timeouts.FORTY_SECONDS, 'Text box user Id not existing');
    }

    public isPasswordTextBoxExisting(): boolean {
        return waitUntil(() => this.password.isExisting(), Timeouts.FORTY_SECONDS, 'Text box password not existing');
    }
}