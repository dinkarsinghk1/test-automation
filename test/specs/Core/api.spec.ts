import chai from 'chai';
import chaiHttp from 'chai-http';
chai.use(chaiHttp);
chai.should();

describe("POST /api/tasks", () => {
    it("It should POST a new task", (done) => {
        const obj = {
            "status": 1,
            "id": 1,
        }
        var url = 'https://reqres.in';
        chai.request(url)
            .post("/api/users?page=2")
            .send(obj)
            .end((err, response) => {
                console.log(response)
                response.should.have.status(201);
                // response.body.should.be.a('object');
                // response.body.should.have.property('id').eq(4);
                // response.body.should.have.property('name').eq("Task 4");
                // response.body.should.have.property('completed').eq(false);
                done();
            });
    });
});