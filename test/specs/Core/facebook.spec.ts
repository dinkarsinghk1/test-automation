import { expect } from 'chai';
import { CoreHomePage } from '../../../src/pages/FbPages/coreHomePage/coreHome.page';
import { TestBuildingBlocks } from '../../../src/infra/utilities/testBuildingBlock';
import { TestLogger } from '../../../src/infra/loggers/test-logger';
import { Facebook } from '../../../src/pages/FbPages/facebook/facebook.page';
import allureReporter from '@wdio/allure-reporter';
import { projConfig } from '../../../src/infra/resources/projConfig';
import { LoginDetails } from "../../../src/infra/models/login-detail";
import { BaseTestData } from "../../../src/infra/models/base-test-data";
import { JellycoreTestBuildingBlocks } from '../../../src/infra/utilities/jellycore-test-buildingBlock';
import { jellyCoreConfig } from '../../../src/infra/config/jellyCore.config';
const path = require('path');

let facebook = new Facebook();
let logger: TestLogger;
let testIndex = 0;
let testData: BaseTestData = new BaseTestData(undefined);
let jellycoreTestBuildingBlocks = new JellycoreTestBuildingBlocks(testData);


describe('Facebook test cases', () => {

    before(() => {
        logger = new TestLogger();
        testData = new BaseTestData(new LoginDetails(jellyCoreConfig.JellycoreEmail, jellyCoreConfig.JellycorePassword));
        jellycoreTestBuildingBlocks = new JellycoreTestBuildingBlocks(testData);
        facebook = jellycoreTestBuildingBlocks.navigateToFBPage();
    });

    beforeEach(() => {
        testIndex++;
        facebook = jellycoreTestBuildingBlocks.navigateToFBPage();
    });

    it('Verify that user ID and Password text box displaying', () => {
        facebook.clickOnFaceBookPopUp();
        TestBuildingBlocks.addStepAndExecute(`Check user Id text box is displaying`, () => {
            expect(facebook.isUserIdTextBoxExisting()).to.equal(true, 'No home page displaying');
        });
        TestBuildingBlocks.addStepAndExecute(`Verify that home page is displaying`, () => {
            expect(facebook.isPasswordTextBoxExisting()).to.equal(true, 'Home page menus are not displaying');
        });
    });
});
